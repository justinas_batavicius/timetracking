var startTime = 0;
var start = 0;
var end = 0;
var diff = 0;
var timerID = 0;

function chrono() {
    end = new Date();
    diff = end - start;
    diff = new Date(diff);
    var msec = diff.getMilliseconds();
    var sec = diff.getSeconds();
    var min = diff.getMinutes();
    var hr = diff.getHours() - 2;
    if (min < 10) {
        min = "0" + min
    }
    if (sec < 10) {
        sec = "0" + sec
    }
    if (msec < 10) {
        msec = "00" + msec
    }
    else if (msec < 100) {
        msec = "0" + msec
    }
    document.getElementById("chronotime").innerHTML = hr + ":" + min + ":" + sec;
    timerID = setTimeout("chrono()", 10)
}

chronoStart = function () {
    document.getElementById("typeheadbtn").onclick = chronoStop;
    document.getElementById("typeheadbtn").onclick = chronoStop;
    start = new Date();
    chrono()
};

chronoContinue = function() {
    document.getElementById("typeheadbtn").onclick = chronoStop;
    start = new Date() - diff;
    start = new Date(start);
    chrono()
}

chronoStopReset = function() {
    document.getElementById("chronotime").innerHTML = "0:00:00";
    document.getElementById("typeheadbtn").onclick = chronoStart
}

chronoStop = function() {
    document.getElementById("typeheadbtn").onclick = chronoContinue;
    document.getElementById("chronotime").innerHTML = "0:00:00";
    clearTimeout(timerID)
}