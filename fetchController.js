angular.module('httpExample', ['inputDropdown', 'ui.bootstrap'])
    .controller('FetchController', ['$scope', '$http', '$templateCache', '$filter', '$timeout',
            function ($scope, $http, $templateCache, $timeout) {
                $scope.currentTimer = {
                    time: 0,
                    runing: false
                };

                $scope.data = [];
                $scope.timeentries = [];
                var moment = require('moment');
                $scope.currentTask = "";
                $scope.currentProject = "";
                $scope.quantity = 1; // Kiek kartu ng-repeat for totalTime

                window.onload = function () {
                    "use strict";

                    //if ($scope.tempresponse[0] == "0000-00-00 00:00:00"){
                    //    $scope.temp();
                    //}
                    $scope.temp();
                    $scope.currentTimer.runing = false;
                    $scope.reloadProject();
                    $scope.fetchViskas(); //task ir project
                    $scope.fetchTask();
                };

                $scope.url = 'http://tt.cpartner.lt/public/api/projects';
                $scope.taskurl = 'http://tt.cpartner.lt/public/api/task';
                $scope.timeEntryurl = 'http://tt.cpartner.lt/public/api/task/save';

                $scope.fetchViskas = function () {
                    $http.get('http://tt.cpartner.lt/public/api/viskas').then(function (response) {
                        "use strict";
                        $scope.status = response.status;
                        $scope.Projecstdata = response.data; // Viskas

                        $scope.modelOptions = {
                            debounce: {
                                default: 500,
                                blur: 250
                            },
                            getterSetter: true
                        };

                        for (var $searchtask in $scope.Projecstdata) {
                            for (var $searchtaskindex in $scope.Projecstdata[$searchtask].task)
                                $scope.arrayseacrh.push(
                                    {
                                        title: $scope.Projecstdata[$searchtask].task[$searchtaskindex].title,
                                        id: $scope.Projecstdata[$searchtask].task[$searchtaskindex].id
                                    })
                        }

                    })
                };
                $scope.masyvas = [];

                $("#projectVardas").keyup(function () {
                    console.log($(this).val().length);
                    if ($(this).val().length)
                        $("#startstop").prop('disabled', false);
                    else
                        $("#startstop").prop('disabled', true);
                });


                $scope.fetchIflygu = function () {
                    $http.get('http://tt.cpartner.lt/public/api/viskaslygu').then(function (response) {
                        "use strict";
                        $scope.status = response.status;
                        $scope.data1 = response.data[0]; // task
                        $scope.data2 = response.data[1]; // project
                        console.log($scope.data);
                    })
                };

                $scope.fetch = function () {
                    $scope.code = null;
                    $scope.response = null;
                    $http({method: "GET", url: $scope.url, cache: $templateCache}).then(function (response) {//if okay
                        $scope.status = response.status;
                        $scope.data = response.data;
                        console.log($scope.data);

                    }, function (response) {//if failed,
                        $scope.data = response.data || "Request failed";
                        $scope.status = response.status;
                    });
                };

                $scope.fetchTask = function () {
                    $scope.code = null;
                    $scope.response = null;
                    $http({method: "GET", url: $scope.taskurl, cache: $templateCache}).then(function (response) {//if okay
                        $scope.status = response.status;
                        $scope.data = response.data;
                        console.log($scope.data);
                    }, function (response) {//if failed
                        $scope.alltasks = data;
                        $scope.status = response.status;
                    });
                };

                $scope.arrayseacrh = [];

                $scope.selectedDropdownItem = null;
                $scope.defaultDropdownObjects = [];


                $scope.fetchTimeEntry = function () {
                    "use strict";
                    $scope.code = null;
                    $scope.response = null;
                    $http({
                        method: "GET",
                        url: $scope.timeEntryurl,
                        cache: $templateCache
                    }).then(function (response) {
                        $scope.status = response.status;
                        $scope.data = response.data;
                        console.log($scope.data);
                    }, function (response) {
                        $scope.data = response.data || "Rquest failed";
                        $scope.status = response.status;
                    });
                };

                $scope.postTimeEntry = function (timeEntryData) {
                    "use strict";
                    $scope.code = null;
                    $scope.response = null;
                    $http({
                        method: "POST",
                        url: $scope.timeEntryurl,
                        data: timeEntryData,
                        cache: $templateCache
                    }).then(function (response) {
                        $scope.status = response.status;
                        $scope.data = response.data;
                        console.log("timeEntryData: " + timeEntryData);
                    }, function (response) {
                        $scope.data = response.data || "Request failed";
                        $scope.status = response.status;
                        console.log("error");
                    })
                };

                $scope.post = function (data) {
                    $scope.code = null;
                    $scope.response = null;
                    $http({method: "POST", url: $scope.url, data: data, cache: $templateCache}).then(function (response) {  //if okay
                        $scope.status = response.status;
                        $scope.data = response.data;
                        console.log($scope.data);
                        console.log("scope post ");

                    }, function (response) { // if failed
                        $scope.data = response.data || "Request failed";
                        $scope.status = response.status;
                        console.log("error");
                    });
                };

                $scope.posttask = function (taskdata) {
                    $scope.code = null;
                    $scope.response = null;
                    $http({
                        method: "POST",
                        url: $scope.taskurl,
                        data: taskdata,
                        cache: $templateCache
                    }).then(function (response) {
                        $scope.status = response.status;
                        $scope.data = response.data;
                        console.log("posttask");
                        console.log(taskdata);
                    }, function (response) {
                        $scope.data = response.data || "Request failed";
                        $scope.status = response.status;
                        console.log("error");
                    });
                };

                $scope.reloadProject = function () {
                    $http.get('http://tt.cpartner.lt/public/api/projects').success(function (data) {
                        $scope.data = data;
                    });
                };

                $scope.updateID = function (element) {
                    "use strict";
                    $http.post('http://tt.cpartner.lt/public/api/getProjectEdit', {
                        'id': element.id,
                        'name': element.name
                    }).success(function (data) {
                        console.log(data);
                    });
                };

                $scope.updateTaskName = function (TaskRepeat) {
                    "use strict";
                    $http.post('http://tt.cpartner.lt/public/api/task/updatename', {
                        'id': TaskRepeat.id,
                        'title': TaskRepeat.title
                    }).success(function (data) {
                        console.log(data);
                    });
                };

                $scope.updateSoloTaskName = function (taskFetch, $index) {
                    "use strict";
                    console.log(taskFetch.title);
                    $http.post('http://tt.cpartner.lt/public/api/task/updatename', {
                        'id': taskFetch.id,
                        'title': document.getElementById("soloTask" + $index).value
                    }).success(function (data) {
                        console.log("data", data);
                        taskFetch.title = data.title;
                    })
                };

                $scope.projectID = function (PIData) {
                    $scope.code = null;
                    $scope.response = null;
                    $http({
                        method: "POST",
                        url: $scope.taskurl,
                        data: PIData,
                        cache: $templateCache
                    }).then(function (response) {  //if okay
                        $scope.status = response.status;
                        $scope.data = response.data;
                        console.log("posttaskPID");
                        console.log(taskdata);
                    }, function (response) { // if failed
                        $scope.data = response.data || "Request failed";
                        $scope.status = response.status;
                        console.log("errorPID");
                    });
                };

                $scope.getProjectId = function () {
                    $http.get('http://tt.cpartner.lt/public/api/task/ID').success(function (data) {
                        $scope.data = data;
                        console.log(data);
                    });
                };

                $scope.updateProject = function () {
                    "use strict";
                    console.log('Project name changed');

                    // TODO: neleisti daug uzklausu

                    $http.get('http://tt.cpartner.lt/public/api/projects/' + $scope.currentProject).success(function (response) {
                        $scope.suggestedProjects = response;
                    });
                };

                $scope.updateTasks = function () {
                    "use strict";
                    console.log('task name changed');

                    // TODO: neleisti daug uzklausu

                    $http.get('http://tt.cpartner.lt/public/api/task/save' + $scope.currentTask).success(function (response) {
                        $scope.suggestedTasks = response;
                    });
                };

                $scope.postTask = function (data) {
                    $scope.code = null;
                    $scope.response = null;
                    $http({
                        method: "POST",
                        url: 'http://tt.cpartner.lt/public/api/task/save',
                        data: data,
                        cache: $templateCache
                    }).then(function (response) {  //if okay
                        $scope.status = response.status;
                        $scope.data = response.data;
                        console.log($scope.data);
                    });
                };

                $scope.timeentryssSoloTask = function (taskFetch, $index) {

                    if ($scope.currentTimer.runing == false) {
                        console.log(taskFetch);
                        $scope.chronoStart();
                        $scope.selected = taskFetch;

                        $scope.currentTimer.runing = true;

                        console.log($scope.data[$index].taskTotal);

                        taskFetch.totalTime = $scope.data[$index].taskTotal;
                        $scope.singlerepeat = setInterval(function () {
                            $scope.$apply(function () {
                                taskFetch.taskTotal += 1;
                            })
                        }, 1000);

                        $http.post('http://tt.cpartner.lt/public/api/timeentry', {
                            start: moment().add(2, 'hours'),
                            end: "0000-00-00 00:00:00",
                            task_id: $scope.selected.id,
                            project_id: $scope.selected.project_id
                        });

                    } else {
                        $scope.chronoStop();
                        clearTimeout(timerID);
                        clearTimeout($scope.singlerepeat);
                        $scope.currentTimer.runing = false;

                        $http.post('http://tt.cpartner.lt/public/api/updateTimeEntry', {
                            end: moment().add(2, 'hours'),
                            task_id: $scope.selected.id,
                            project_id: $scope.selected.project_id
                        });

                        $http.post('http://tt.cpartner.lt/public/api/getTaskTotal', {
                            task_id: $scope.selected.id
                        })
                    }

                };

                $scope.getTaskTotal = function (TaskRepeat, element, selected, $index) {

                    $scope.selected = TaskRepeat;

                    if ($scope.currentTimer.runing == false) {
                        $scope.chronoStart();
                        $scope.currentTimer.runing = true;
                        $('#spanid').removeClass('glyphicon-play').addClass('glyphicon-stop');
                    } else {
                        $scope.currentTimer.runing = false;
                        $scope.chronoStop();
                        $('#spanid').removeClass('glyphicon-stop').addClass('glyphicon-play');
                    }

                    var moment = require('moment');

                    if ($scope.currentTimer.runing) {
                        console.log("run forest");
                        $http.post('http://tt.cpartner.lt/public/api/timeentry', {
                            start: moment().add(2, 'hours'),
                            title: TaskRepeat.title,
                            task_id: TaskRepeat.id,
                            project_id: TaskRepeat.project_id,
                            end: "00:00:00"
                        })
                    } else {
                        console.log("stop forest");
                        // post time entry
                        $http.post('http://tt.cpartner.lt/public/api/getTaskTotal', {
                            id: TaskRepeat.id,
                            end: moment().add(2, 'hours')
                        }).then(function () {
                            // get total project time
                            $http.post('http://tt.cpartner.lt/public/api/totalTime', {
                                project_id: TaskRepeat.project_id
                            }).success(function (data) {
                                $scope.selected.totalTime = data;
                            });
                        });
                    }

                    if ($scope.currentTimer.runing == false) {
                        $http.post('http://tt.cpartner.lt/public/api/getTaskTotal', {
                            task_id: TaskRepeat.id
                        }).success(function (data) {
                            console.log(data);
                            $scope.taskTotal = data;
                        });
                    }
                };

                $scope.delete = function (TaskRepeat, element, $index) {
                    console.log("ElementId:", TaskRepeat.project_id);
                    $http.post('http://tt.cpartner.lt/public/api/task/delete', {
                        'id': TaskRepeat.id,
                        'project_id': TaskRepeat.project_id
                    }).success(function (response) {
                        $index.task = response[0];
                        $scope.data = response[1];
                        console.log("delete data", response);
                    });
                };

                $scope.deleteSoloTask = function (taskFetch, $index) {
                    $http.post('http://tt.cpartner.lt/public/api/task/deletegetall', {
                        'id': taskFetch.id,
                        'project_id': 0
                    }).success(function (response) {
                        //taskFetch = response;
                        $scope.data = response;
                        //$scope.data.push(response);
                    })
                };

                $scope.pateiktiTask = function (element, $index) {
                    $http.post('http://tt.cpartner.lt/public/api/task/new', {
                        project_id: element.id,
                        title: document.getElementById("task-" + $index).value
                    }).success(function (data) {
                        $http.get('http://tt.cpartner.lt/public/api/task');
                        console.log($scope.Projecstdata[$index]);
                        console.log($scope.Projecstdata);
                        //$scope.Projecstdata[$index].push(data);
                        //[projectj][task].push(data)
                        $scope.selected = data;
                        $scope.selected.taskTotal = 0;
                        $scope.Projecstdata[$index].task.push(data);
                        //$scope.data.push(data);
                    });
                };

                $scope.pateiktiSoloTask = function () {
                    $http.post('http://tt.cpartner.lt/public/api/task/new', {
                        project_id: 0,
                        title: document.getElementById("newSoloTask").value
                    }).success(function (data) {
                        console.log(data);
                        $scope.data.push(data);
                    });
                };

                $scope.postProjectTask = function () {

                    document.chronoForm.reset.onclick = new Date();
                    clearTimeout(timerID);
                    $scope.currentTimer.runing = false;
                    $http.post('http://tt.cpartner.lt/public/api/projects', {
                        name: document.getElementById("projectVardas").value
                    }).success(function (data) {
                        "use strict";
                        $scope.Projecstdata.push(data);
                    });
                };

                $scope.renameProject = function (element, $index) {
                    $http.post('http://tt.cpartner.lt/public/api/projects/renameproject', {
                        name: document.getElementById("rename" + $index).value,
                        id: element.id
                    }).success(function (data) {
                        $scope.Projecstdata = data;
                    });
                };

                $scope.deleteProject = function (element) {
                    $http.post('http://tt.cpartner.lt/public/api/projects/deleteproject', {
                        id: element.id,
                        projectid: element.project_id
                    }).success(function (response) {
                        "use strict";
                        console.log("delte data: ", response);
                        $scope.Projecstdata = response;
                    })

                };

                $scope.selectedTask = '';

                $scope.temp = function () {

                    $http.get('http://tt.cpartner.lt/public/api/temppor', {}).success(function (response) {
                        //$scope.tempresponse = response;
                        $scope.selected = response[1][0];
                        console.log($scope.tempresponse);

                        if (response[0] == "0000-00-00 00:00:00" && response[1].length != 0) {
                            $scope.currentTimer.runing = true;

                            $('#spanid').removeClass('glyphicon-play').addClass('glyphicon-stop');
                            $('#typeheadbtn').click(function () {
                                $(this).find('span').toggleClass('glyphicon-play')
                                    .toggleClass('glyphicon-stop');
                            });

                            $scope.pradzios = moment(response[2]);
                            $scope.dabartinis = moment().add(2, 'hours');

                            $scope.skaiciavimas = $scope.dabartinis.diff($scope.pradzios, 'seconds');

                            $scope.min = 0;
                            $scope.val = 0;

                            $scope.tempdata = $scope.skaiciavimas;
                            $scope.currentTimer.time = $scope.skaiciavimas;

                            $scope.unfinishedTask = setInterval(function () {
                                $scope.$apply(function () {

                                    $scope.currentTimer.time += 1000;
                                    console.log($scope.currentTimer.time);
                                })
                            }, 1000);

                            $('#typeheadbtn').click(function () {
                                console.log(response[1][0].id);
                                $http.post('http://tt.cpartner.lt/public/api/updatetimeentrytypehead', {
                                    id: response[1][0].id,
                                    end: moment().add(2, 'hours')
                                });
                                document.getElementById("chronotime").innerHTML = "00:00:00";
                            });

                            response[0] = moment().format("YYYY-MM-DD HH-mm-ss");

                        } else {
                            $('#typeheadbtn').click(function () {
                                $(this).find('span').toggleClass('glyphicon-play')
                                    .toggleClass('glyphicon-stop');
                            });
                            $scope.currentTimer.runing = false;
                        }

                    });
                };


                $scope.typeheadstart = function (selected) {

                    console.log("selected",$scope.selected);

                    if ($scope.currentTimer.runing == false) {
                        $scope.currentTimer.runing = true;
                        clearTimeout($scope.unfinishedTask);
                    } else {
                        clearTimeout($scope.unfinishedTask);
                        $scope.currentTimer.runing = false;
                    }

                    $scope.objektas = $scope.selected.id;
                    $scope.currentTimer.taskid = selected.id;


                    if (angular.element(spanid).hasClass('glyphicon glyphicon-play')) {
                        $scope.chronoStart();

                        if (typeof selected == "object") {
                            if ($scope.currentTimer.runing == true) {
                                $http.post('http://tt.cpartner.lt/public/api/timeentrytypehead', {
                                    start: moment().add(2, 'hours'),
                                    project_id: selected.project_id,
                                    id: selected.id,
                                    title: selected.title
                                });
                            } else {
                                $http.post('http://tt.cpartner.lt/public/api/updatetimeentrytypehead', {
                                    id: selected.id,
                                    end: moment().add(2, 'hours')
                                }).then(function () {
                                    $http.post('http://tt.cpartner.lt/public/api/getTaskTotal', {
                                        task_id: selected.id
                                    }).success(function (data) {
                                        $scope.data.push(data);
                                    });
                                    $http.post('http://tt.cpartner.lt/public/api/totalTime', {
                                        project_id: selected.project_id
                                    }).success(function (data) {
                                        $scope.selected.totalTime = data;

                                    });
                                });
                            }
                        } else {
                            if ($scope.currentTimer.runing && document.getElementById("searchtypehead").value != "" || " ") {

                                console.log(document.getElementById("searchtypehead").value);
                                console.log(document.getElementById("searchtypehead").innerHTML);
                                $http.post('http://tt.cpartner.lt/public/api/task/new', {
                                    title: selected,
                                    project_id: 0
                                }).success(function (response) {
                                    $scope.data.push(response);
                                    $scope.selected = response;
                                }).then(function (atsakymas) {
                                    $http.post('http://tt.cpartner.lt/public/api/timeentrytypehead', {
                                        start: moment().add(2, 'hours'),
                                        project_id: atsakymas.data.project_id,
                                        id: atsakymas.data.id,
                                        title: atsakymas.data.title
                                    })
                                });
                            } else {
                                console.log("else");
                                $http.post('http://tt.cpartner.lt/public/api/updatetimeentrytypehead', {
                                    id: selected.id,
                                    end: moment().add(2, 'hours')
                                });
                            }
                        }

                    } else {
                        $scope.chronoStop();
                        clearTimeout();
                        clearTimeout($scope.taskrepeat);
                    }


                    //}
                };

                $scope.ngstartTime = 0;
                $scope.ngstart = 0;
                $scope.ngend = 0;
                $scope.ngdiff = 0;
                $scope.ngtimerID = 0;

                $scope.ngchrono = function () {

                    $scope.ngtimerID = setInterval(function () {
                        $scope.$apply(function () {
                            $scope.currentTimer.time += 1000;
                        })
                    }, 1000);
                };

                $scope.chronoStart = function (task) {
                    document.getElementById("typeheadbtn").onclick = $scope.chronoStop;
                    $scope.ngstart = new Date();
                    $scope.ngchrono();
                };

                $scope.chronoContinue = function () {
                    document.getElementById("typeheadbtn").onclick = $scope.chronoStop;
                    $scope.ngstart = new Date() - $scope.ngdiff;
                    $scope.ngstart = new Date($scope.ngstart);
                    $scope.ngchrono()
                };

                $scope.chronoStopReset = function () {
                    $scope.currentTimer.time = 0;
                    document.getElementById("typeheadbtn").onclick = $scope.chronoStart
                };

                $scope.chronoStop = function () {
                    document.getElementById("typeheadbtn").onclick = $scope.chronoContinue;
                    $scope.currentTimer.time = 0;
                    clearTimeout($scope.ngtimerID)
                };

            }],
        angular.module('httpExample').directive('startstop', function () {
            return {
                restrict: 'E',
                replace: true,
                template: '<input type="button" value="start" class="btn button-default startbtn">',
                scope: {
                    task: '=',
                    timer: '=',
                    source: '=',
                    selectedid: '=',
                    taskid: '=',
                    smth: '='
                }, link: function ($scope, elem, attrs, selected) {

                    $scope.$watch(function () {
                        return $scope.timer.runing;
                    }, function (newval) {
                        if (newval == true) {
                            if ($scope.selectedid.id != $scope.task.id) {
                                elem.val("start");
                                angular.element(elem).addClass('disabled');
                            } else {
                                console.log("selectedid:", $scope.selectedid.id, " task.id:", $scope.task.id);
                                angular.element(elem).removeClass('disabled');

                                $scope.taskrepeat = setInterval(function () {
                                    $scope.$apply(function () {
                                        $scope.smth.totalTime += 1;
                                        $scope.task.taskTotal += 1;
                                    })
                                }, 1000);

                                elem.val("stop");
                            }
                        } else {
                            angular.element(elem).removeClass('disabled');
                            clearTimeout($scope.taskrepeat);
                            elem.val("start");
                        }
                    });
                }
            }
        })
    );