'use strict';
const electron = require('electron');
const ipcMain = require('electron').ipcMain;
var ipc = require("electron").ipcRenderer;
const app = electron.app;  // Module to control application life.
const BrowserWindow = electron.BrowserWindow;  // Module to create native browser window.
const globalShortcut = electron.globalShortcut;
var request = require('request');
let mainWindow;

let kazkas;



app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform != 'darwin') {
        app.quit();
    }
});


//This method will be called when Electron has finished
//initialization and is ready to create browser windows.
app.on('ready', function () {
    // Register a 'ctrl+x' shortcut listener.

    mainWindow = new BrowserWindow({width: 1000, height: 600, preload: "./fetchController.js" });

    ipcMain.on('asynchronous-message', function(event, arg) {
        console.log(arg);  // prints "ping"
        event.sender.send('asynchronous-reply', 'ASYNC');
    });
    ipcMain.on('synchronous-message', function(event, arg) {
        console.log(arg);  // prints "ping"
        event.returnValue = 'SYNC';
    });
    mainWindow.webContents.send('ping', 'whoooooooh!');
    mainWindow.loadURL('file://' + __dirname + '/index.html');

    mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        mainWindow = null;
    });

});



